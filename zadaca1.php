<?php

//Skripta za PHP site funkcii i proverki se tuka na edno mesto
function ConvertDecadeToBinary($num){
    //decaade numbers are 10,20,30,40,50...
    $reserved=[];
    if($num>3999 || $num<1){
        echo "Error number!";

    }else{
       
        while($num>0){//212
          
            $remainder = $num % 2; // 30 % 2 //212%2=0
            
            // echo " Brojot e $num <br/>";
         
            // echo" Brojot e $num, a Ostatokot e $remainder <br/>";
            $num = (int)($num/2); //30/2=15
        
           
            array_unshift($reserved,$remainder);

        }
    }
    for($i = 0; $i < count($reserved); $i++){
        echo  "" . $reserved[$i];
    }
    echo "<br/>";
    
}
//recurzivna funckija za konverzija od binaren vo dekaden
function RecursiveConvertBinaryToDecade($num,$i,$reversed){//110101001//1,0,reversed
    // echo "Length of string is $i<br/>";

    
    // echo "Num is " .$num."<br/>";
    // $str=(string)$num;//111001
    // echo $str."<br/>";
    if($i < 0){
        // echo "Basic Length of string is $i<br/>";
        // echo "decimal number is ". $reversed;
        return $reversed."<br/>";
        // echo $re."<br/>";
    }
    // echo "num[0]" . $num[0]."<br/>";
    // echo substr($num,1)."<br/>";
    $reversed = $reversed + (int)($num[0])*pow(2,$i);//
    // echo "Stepenot e  ". (int)($num[0])*pow(2,$i) ."<br/>";
    // echo "Reversed is: " . $reversed. "<br/>";
    return RecursiveConvertBinaryToDecade(substr($num,1),
   
    $i-1,
    $reversed);
    //
   
}
// echo substr("happy",1);
function ConvertBinaryToDecade($num){
    //decaade numbers are 10,20,30,40,50...
    $str=(string)$num;//111001
    $decade_num=0;
    for($i=strlen($str)-1;$i>=0;$i--){
        //$i=5,4,3,2,1,0;
        // echo "Stepenot e " .pow(2,$i)."<br/>";
        $decade_num = $decade_num + (int)($str[strlen($str)-1-$i])*pow(2,$i);
       
    



    }

 
    if($decade_num > 3999){
        return "Ve molam vnesete drug binaren kod";
    }else{
        return $decade_num."<br/>";
    }

   

 
    
}
//konverzija na decimalen vo roman broj 
function convertDecadeToRoman($num){
    //
    // echo $num;
    $num=(int)$num;
    // echo $num;
    // $niza=[10=>'X',50=>'L',100=>'C',500=>'D',1000=>'M'];
    if($num>3999999 || $num<1){//increased limit for roman numbers 1st. condition 3999
        echo "Error number!";

    }else{
        while($num > 0){
            if($num>=1000000)
            {
               
                echo "m";//mmm
                $num-=1000000;
                // echo $num;
            }else if($num>=900000){
                echo "cm";
                $num-=900000;
            }

            else if($num>=500000){
                echo 'd';
                $num-=500000;
                // echo $num;
            }
            else if($num>=400000){
                echo "cd";
                $num-=400000;
            }
            else if($num>=100000){
                echo "c";
                $num-=100000;

            }
            else if($num>=90000){
                echo "xc";
                $num-=90000;
            }
            else if($num >= 50000){
                echo "l";
                $num-=50000;
            }
            else if($num>=40000){
                echo "xl";
                $num-=40000;
            }
            else if($num>=10000){
                echo "x";
                $num-=10000;
            }else if($num>=9000){
                echo "Mx";
                $num-=9000;
            }
            // echo $num."<br/>";
            else if($num>=5000){
                echo "v";
                $num-=5000;
                // echo $num."<br/>";
            }

            else if($num>=4000){
                echo 'iv';
                $num-=4000;
            }

           else if ($num >= 1000)    
            {
               echo "M";
               $num -= 1000;
            }
    
            else if ($num >= 900)   
            {
               echo "CM";
               $num = $num - 900;//
            }        
    
            else if ($num >= 500)  
            {           
               echo "D";
               $num -= 500;
            }
    
            else if ($num >= 400)  
            {
               echo "CD";
               $num -= 400;
            }
    
            else if ($num >= 100)  //125
            {
               echo "C";
               $num -= 100;     //125=125-100//25                  
            }
    
            else if ($num >= 90) 
            {
               echo "XC";
               $num -= 90;                                              
            }
    
            else if ($num >= 50)  
            {
               echo "L";
              $num -= 50;                                                                     
            }
    
            else if ($num >= 40)   
            {
               echo "XL";           
               $num -= 40;
            }
    
            else if ($num >= 10)  
            {
               echo "X";
              $num -= 10;     
            //   echo $num."<br/>";      
            }
    
            else if ($num >= 9)  
            {
               echo "IX";
               $num -= 9;                         
            }
    
            else if ($num >= 5)    
            {
               echo "V";
               $num -= 5;                                     
            }
    
            else if ($num >= 4)   
            {
               echo "IV";
               $num -= 4;                                                            
            }
    
            else if ($num >= 1)     
            {
               echo "I";
               $num -= 1;                                                                                   
            }
            
        }
    }
    // return 0;

}
//konverzija na roman vo decimalen broj 
function convertRomanToDecade($num){//200 is cc 300 ccc,2000 mm
    //so istata logika od gore XLMMMCDD
//    echo "Bukvata e " + $num[0];
    //  echo $num;//mmmcmxcMxCMXCIX
//    echo substr($num,0,1)=='m';
    if ($num==' ') {
        return 0;
    }
    if(substr($num,0,1)=='m'){//moze i _M i M so crta nad nego(Vaka e polesno)
        // echo $num;
        return 1000000 + convertRomanToDecade(substr($num,1));
        
    }
    else if(substr($num,0,2)=='cm'){
        return 900000 + convertRomanToDecade(substr($num,2));
    }
    else if(substr($num,0,1)=='d'){
        return 500000 + convertRomanToDecade(substr($num,1));
    }
    else if(substr($num,0,2)=='cd'){
        return 400000 + convertRomanToDecade(substr($num,2));
    }
    else if(substr($num,0,1)=='c'){
        return 100000 + convertRomanToDecade(substr($num,1));
    }
    else if(substr($num,0,2)=='xc'){
        return 90000 + convertRomanToDecade(substr($num,2));
    }
    else if(substr($num,0,1)=='l'){
        return 50000 + convertRomanToDecade(substr($num,1));
    }
    else if(substr($num,0,2)=='xl'){
        return 40000 + convertRomanToDecade(substr($num,2));
    }
    else if(substr($num,0,1)=='x'){
        return 10000 + convertRomanToDecade(substr($num,1));
    }
    else if(substr($num,0,2)=='Mx'){
        return 9000 + convertRomanToDecade(substr($num,2));
    }
    else if(substr($num,0,1)=='v'){
        return 5000 + convertRomanToDecade(substr($num,1));
    }
    else if(substr($num,0,2)=='iv'){
        return 4000 + convertRomanToDecade(substr($num,2));
    }
    else if(substr($num,0,1)=='M'){
        return 1000 + convertRomanToDecade(substr($num,1));
    }
    else if(substr($num,0,2)=='CM'){
        return 900 + convertRomanToDecade(substr($num,2));
    }
    else if(substr($num,0,1)=='D'){
        return 500 + convertRomanToDecade(substr($num,1));
    }
    else if(substr($num,0,2)=='CD'){
        return 400 + convertRomanToDecade(substr($num,2));
    }
    else if(substr($num,0,1)=='C'){
        return 100 + convertRomanToDecade(substr($num,1));
    }
    else if(substr($num,0,2)=='XC'){
        return 90 + convertRomanToDecade(substr($num,2));
    }
    else if(substr($num,0,1)=='L'){
        return 50 + convertRomanToDecade(substr($num,1));
    }
    else if(substr($num,0,2)=='XL'){
        return 40 + convertRomanToDecade(substr($num,2));
    }
    else if(substr($num,0,1)=='X'){
        return 10 + convertRomanToDecade(substr($num,1));
    }
    else if(substr($num,0,2)=='IX'){
        return 9 + convertRomanToDecade(substr($num,2));
    }
    else if(substr($num,0,1)=='V'){
        return 5 + convertRomanToDecade(substr($num,1));
    }
    else if(substr($num,0,2)=='IV'){
        return 4 + convertRomanToDecade(substr($num,2));
    }
    else if(substr($num,0,1)=='I'){
        return 1 + convertRomanToDecade(substr($num,1));
    }
    


}
//check if number is decimal, binary or roman
function CheckTypeOfNumber($num){//-11101
    //If number is greater than 3999 print an error message.
    // $roman_letters=['I','V','X','L','C','M'];
    $str=(string)$num;
    $bin=true;
    $roman=true;
    $string='';
    // echo $str;
    //first for binary
    for($i=0;$i<strlen($str);$i++){
        if($str[$i]=='1' or $str[$i]=='0'){
            //   echo $str[$i] ."<br/>";
               $bin=true;

        // }else if($str[$i]=='I' or $str[$i]=='V' or  $str[$i]=='X' or  $str[$i]=='L' or $str[$i]=='C' or $str[$i]=='D' or $str[$i]=='M'){
        //     $roman=true;

        }
        else{
            $bin=false;
            $roman=false;
            break;
        }

    }
      if($bin==true){
        //   $str=$num . ' is binary <br/>';
          return "binary";
      }
      else{
       
      for($i=0;$i<strlen($str);$i++){
                    if(strtoupper($str[$i])=='I'
                     or strtoupper($str[$i])=='V' 
                     or strtoupper($str[$i])=='X' 
                     or strtoupper($str[$i])=='L' 
                     or strtoupper($str[$i])=='C' 
                     or strtoupper($str[$i])=='D' 
                     or strtoupper($str[$i])=='M'
                    ){
                        $roman=true;
                }
                    else{
                    
                        $roman=false;
                        break;
                    }

    }
    if($roman==true){
        // $str=$num .' is roman<br/>';
        return "roman";
    }
    else{
       if(is_numeric($num))
            {
                // $str=$num .' is decimal<br/>';
                // echo $str;
                return "decimal";
            }  else{
                $str=$num .' ne e decimal,binary or roman<br/>';
                return 0;
            }  

}
}


}
function CheckSign($num){
     //$num e strin, a $Num1 e (int)stringot
     
    //  if(CheckTypeOfNumber($num)=="decimal"){
    //     $int_num=(int)$num;
    //     echo $int_num;
    //     echo CheckTypeOfNumber($int_num);
    //     $string_num=(string)$num;
    //     if($string_num[0]=='+' and $string_num[1]=='0'){
    //         echo "Error";
    //     }else if($string_num[0]=='+' or $string_num[0]=='-'){
    //         echo "$num e vo korekten format<br/>";
    //     }else{
        $new_str='';
        if($num[0]=='+' or $num[0]=='-'){
           
            
            for($i=1;$i<strlen($num);$i++){
               $new_str.=$num[$i];
            }
            if(CheckTypeOfNumber($new_str)=="binary"){
                echo "error";
                // break;
            }else if(CheckTypeOfNumber($new_str)=="decimal"){
                if($num[1]=='0'){
                    echo "error";
                    // break;
                }else{
                    return 1;
                }
            }else{
                //super e 
                return 1;
            }
                    
        }
        // }
        // echo $new_str."<br/>";
        // }

    //  }else if(CheckTypeOfNumber($num)=="binary"){
    //      if($num[0]=='+' or $num[0]=='-'){
    //          echo "Error<br/>";
    //      }else{
    //          echo $num;
    //      }
    //  }
  

   

}
$decade_num=425;
$binary_num_first=1101010101;
$binary_num='110101001';
$check_bin='+1110001';
$check_let="asjjs";
$check_roman=strtoupper("MMXIV");//za ako se prakjaat mali bukvi
// echo $check_roman;
$check_decade='+425';
$check_decade_error='+0123';
$check_int_decade=(int)$check_decade;
$check_bin_decade=(string)$binary_num_first;
// // echo $check_decade;
// // echo $check_int_decade;
CheckSign($check_decade_error);
echo "<br/>";
CheckSign($check_bin);
echo "<br/>";
// CheckSign($binary_num_first);
if(CheckSign($check_decade)){//425
  
    echo ConvertDecadeToBinary($check_decade);
    echo "<br/>";
    convertDecadeToRoman($check_decade)."<br/>";
}
echo "<br/>";
echo RecursiveConvertBinaryToDecade((string)$binary_num_first,
strlen((String)$binary_num_first)-1,0);
echo "<br/>";
echo convertRomanToDecade((String)$check_roman);
echo "<br/>";
ConvertDecadeToBinary($decade_num);
echo "<br/>";
echo ConvertBinaryToDecade($binary_num);
echo "<br/>";
$decade_to_roman=2014;
echo convertDecadeToRoman($decade_to_roman);
echo "<br/>";
echo CheckTypeOfNumber($check_bin);//1120002
echo "<br/>";
echo CheckTypeOfNumber($binary_num_first);//110101001
echo "<br/>";
echo CheckTypeOfNumber($binary_num);//11
echo "<br/>";
echo CheckTypeOfNumber($check_roman);//XLMMMCDD
echo "<br/>";
echo CheckTypeOfNumber($decade_num);//425
echo "<br/>";
echo CheckTypeOfNumber($check_let);//ajjsj
echo "<br/>";
$numbers=['+125','1100011','MMXXIV','+345','+3999999','mmmcmxcMxCMXCIX','10101','1011011','CCXXVI','+2301'];
//konvertiraj na nizata
echo "Konvertiranite broevi se: ";
for($i=0;$i< count($numbers);$i++){
    if(CheckTypeOfNumber($numbers[$i])=='binary'){
       echo ConvertBinaryToDecade($numbers[$i]).",";
    }else if(CheckTypeOfNumber($numbers[$i])=='roman'){
        // echo "Roman number is ".$numbers[$i];
        echo convertRomanToDecade($numbers[$i]).",";
    }else if(CheckTypeOfNumber($numbers[$i])=='decimal'){//decade ili decimal
        //(ako e decade samo treba %10???)
        // moze i so ovaa funkcija : echo ConvertDecadeToBinary($numbers[$i])." ";
        // echo $numbers[$i];
        echo convertDecadeToRoman($numbers[$i]).",";

    }
    if($i==count($numbers)-1){
        echo ";";
    }
    
}
// echo convertRomanToDecade('mmmcmxcMxCMXCIX')."<br/>";

?>

